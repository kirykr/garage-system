require 'rails_helper'

RSpec.describe "customers/index", type: :view do
  before(:each) do
    assign(:customers, [
      Customer.create!(
        :name => "Name",
        :compay => "Compay",
        :address => "Address",
        :email => "Email",
        :phone1 => "Phone1",
        :phone2 => "Phone2"
      ),
      Customer.create!(
        :name => "Name",
        :compay => "Compay",
        :address => "Address",
        :email => "Email",
        :phone1 => "Phone1",
        :phone2 => "Phone2"
      )
    ])
  end

  it "renders a list of customers" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Compay".to_s, :count => 2
    assert_select "tr>td", :text => "Address".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
    assert_select "tr>td", :text => "Phone1".to_s, :count => 2
    assert_select "tr>td", :text => "Phone2".to_s, :count => 2
  end
end
