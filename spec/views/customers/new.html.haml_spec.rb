require 'rails_helper'

RSpec.describe "customers/new", type: :view do
  before(:each) do
    assign(:customer, Customer.new(
      :name => "MyString",
      :compay => "MyString",
      :address => "MyString",
      :email => "MyString",
      :phone1 => "MyString",
      :phone2 => "MyString"
    ))
  end

  it "renders new customer form" do
    render

    assert_select "form[action=?][method=?]", customers_path, "post" do

      assert_select "input[name=?]", "customer[name]"

      assert_select "input[name=?]", "customer[compay]"

      assert_select "input[name=?]", "customer[address]"

      assert_select "input[name=?]", "customer[email]"

      assert_select "input[name=?]", "customer[phone1]"

      assert_select "input[name=?]", "customer[phone2]"
    end
  end
end
