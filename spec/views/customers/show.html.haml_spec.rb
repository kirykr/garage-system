require 'rails_helper'

RSpec.describe "customers/show", type: :view do
  before(:each) do
    @customer = assign(:customer, Customer.create!(
      :name => "Name",
      :compay => "Compay",
      :address => "Address",
      :email => "Email",
      :phone1 => "Phone1",
      :phone2 => "Phone2"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Compay/)
    expect(rendered).to match(/Address/)
    expect(rendered).to match(/Email/)
    expect(rendered).to match(/Phone1/)
    expect(rendered).to match(/Phone2/)
  end
end
