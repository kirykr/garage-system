require 'rails_helper'

RSpec.describe "invoices/index", type: :view do
  before(:each) do
    assign(:invoices, [
      Invoice.create!(
        :reference_number => 2,
        :original_price => 3.5,
        :earning_value => 4.5,
        :total_price => 5.5,
        :discount_price => 6.5,
        :deposit_price => 7.5,
        :balance => 8.5,
        :user => nil,
        :customer => nil
      ),
      Invoice.create!(
        :reference_number => 2,
        :original_price => 3.5,
        :earning_value => 4.5,
        :total_price => 5.5,
        :discount_price => 6.5,
        :deposit_price => 7.5,
        :balance => 8.5,
        :user => nil,
        :customer => nil
      )
    ])
  end

  it "renders a list of invoices" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.5.to_s, :count => 2
    assert_select "tr>td", :text => 4.5.to_s, :count => 2
    assert_select "tr>td", :text => 5.5.to_s, :count => 2
    assert_select "tr>td", :text => 6.5.to_s, :count => 2
    assert_select "tr>td", :text => 7.5.to_s, :count => 2
    assert_select "tr>td", :text => 8.5.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
