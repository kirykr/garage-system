require 'rails_helper'

RSpec.describe "invoices/show", type: :view do
  before(:each) do
    @invoice = assign(:invoice, Invoice.create!(
      :reference_number => 2,
      :original_price => 3.5,
      :earning_value => 4.5,
      :total_price => 5.5,
      :discount_price => 6.5,
      :deposit_price => 7.5,
      :balance => 8.5,
      :user => nil,
      :customer => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3.5/)
    expect(rendered).to match(/4.5/)
    expect(rendered).to match(/5.5/)
    expect(rendered).to match(/6.5/)
    expect(rendered).to match(/7.5/)
    expect(rendered).to match(/8.5/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
