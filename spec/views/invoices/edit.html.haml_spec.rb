require 'rails_helper'

RSpec.describe "invoices/edit", type: :view do
  before(:each) do
    @invoice = assign(:invoice, Invoice.create!(
      :reference_number => 1,
      :original_price => 1.5,
      :earning_value => 1.5,
      :total_price => 1.5,
      :discount_price => 1.5,
      :deposit_price => 1.5,
      :balance => 1.5,
      :user => nil,
      :customer => nil
    ))
  end

  it "renders the edit invoice form" do
    render

    assert_select "form[action=?][method=?]", invoice_path(@invoice), "post" do

      assert_select "input[name=?]", "invoice[reference_number]"

      assert_select "input[name=?]", "invoice[original_price]"

      assert_select "input[name=?]", "invoice[earning_value]"

      assert_select "input[name=?]", "invoice[total_price]"

      assert_select "input[name=?]", "invoice[discount_price]"

      assert_select "input[name=?]", "invoice[deposit_price]"

      assert_select "input[name=?]", "invoice[balance]"

      assert_select "input[name=?]", "invoice[user_id]"

      assert_select "input[name=?]", "invoice[customer_id]"
    end
  end
end
