require 'rails_helper'

RSpec.describe "vehicles/show", type: :view do
  before(:each) do
    @vehicle = assign(:vehicle, Vehicle.create!(
      :model_year => "Model Year",
      :model_type => "Model Type",
      :registration_number => "Registration Number",
      :nogears => "Nogears",
      :key_number => "Key Number",
      :enginesize => "Enginesize",
      :engine_number => "Engine Number",
      :gearbox_number => "Gearbox Number",
      :odometerreading => "Odometerreading",
      :price => 2.5,
      :customer => nil,
      :car_model => nil
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Model Year/)
    expect(rendered).to match(/Model Type/)
    expect(rendered).to match(/Registration Number/)
    expect(rendered).to match(/Nogears/)
    expect(rendered).to match(/Key Number/)
    expect(rendered).to match(/Enginesize/)
    expect(rendered).to match(/Engine Number/)
    expect(rendered).to match(/Gearbox Number/)
    expect(rendered).to match(/Odometerreading/)
    expect(rendered).to match(/2.5/)
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
