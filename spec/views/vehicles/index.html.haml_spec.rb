require 'rails_helper'

RSpec.describe "vehicles/index", type: :view do
  before(:each) do
    assign(:vehicles, [
      Vehicle.create!(
        :model_year => "Model Year",
        :model_type => "Model Type",
        :registration_number => "Registration Number",
        :nogears => "Nogears",
        :key_number => "Key Number",
        :enginesize => "Enginesize",
        :engine_number => "Engine Number",
        :gearbox_number => "Gearbox Number",
        :odometerreading => "Odometerreading",
        :price => 2.5,
        :customer => nil,
        :car_model => nil
      ),
      Vehicle.create!(
        :model_year => "Model Year",
        :model_type => "Model Type",
        :registration_number => "Registration Number",
        :nogears => "Nogears",
        :key_number => "Key Number",
        :enginesize => "Enginesize",
        :engine_number => "Engine Number",
        :gearbox_number => "Gearbox Number",
        :odometerreading => "Odometerreading",
        :price => 2.5,
        :customer => nil,
        :car_model => nil
      )
    ])
  end

  it "renders a list of vehicles" do
    render
    assert_select "tr>td", :text => "Model Year".to_s, :count => 2
    assert_select "tr>td", :text => "Model Type".to_s, :count => 2
    assert_select "tr>td", :text => "Registration Number".to_s, :count => 2
    assert_select "tr>td", :text => "Nogears".to_s, :count => 2
    assert_select "tr>td", :text => "Key Number".to_s, :count => 2
    assert_select "tr>td", :text => "Enginesize".to_s, :count => 2
    assert_select "tr>td", :text => "Engine Number".to_s, :count => 2
    assert_select "tr>td", :text => "Gearbox Number".to_s, :count => 2
    assert_select "tr>td", :text => "Odometerreading".to_s, :count => 2
    assert_select "tr>td", :text => 2.5.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
  end
end
