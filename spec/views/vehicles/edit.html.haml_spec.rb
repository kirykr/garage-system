require 'rails_helper'

RSpec.describe "vehicles/edit", type: :view do
  before(:each) do
    @vehicle = assign(:vehicle, Vehicle.create!(
      :model_year => "MyString",
      :model_type => "MyString",
      :registration_number => "MyString",
      :nogears => "MyString",
      :key_number => "MyString",
      :enginesize => "MyString",
      :engine_number => "MyString",
      :gearbox_number => "MyString",
      :odometerreading => "MyString",
      :price => 1.5,
      :customer => nil,
      :car_model => nil
    ))
  end

  it "renders the edit vehicle form" do
    render

    assert_select "form[action=?][method=?]", vehicle_path(@vehicle), "post" do

      assert_select "input[name=?]", "vehicle[model_year]"

      assert_select "input[name=?]", "vehicle[model_type]"

      assert_select "input[name=?]", "vehicle[registration_number]"

      assert_select "input[name=?]", "vehicle[nogears]"

      assert_select "input[name=?]", "vehicle[key_number]"

      assert_select "input[name=?]", "vehicle[enginesize]"

      assert_select "input[name=?]", "vehicle[engine_number]"

      assert_select "input[name=?]", "vehicle[gearbox_number]"

      assert_select "input[name=?]", "vehicle[odometerreading]"

      assert_select "input[name=?]", "vehicle[price]"

      assert_select "input[name=?]", "vehicle[customer_id]"

      assert_select "input[name=?]", "vehicle[car_model_id]"
    end
  end
end
