require 'rails_helper'

RSpec.describe "models/new", type: :view do
  before(:each) do
    assign(:model, Model.new(
      :name => "MyString",
      :version => "MyString",
      :parent_id => 1
    ))
  end

  it "renders new model form" do
    render

    assert_select "form[action=?][method=?]", models_path, "post" do

      assert_select "input[name=?]", "model[name]"

      assert_select "input[name=?]", "model[version]"

      assert_select "input[name=?]", "model[parent_id]"
    end
  end
end
