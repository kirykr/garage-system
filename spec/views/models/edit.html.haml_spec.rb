require 'rails_helper'

RSpec.describe "models/edit", type: :view do
  before(:each) do
    @model = assign(:model, Model.create!(
      :name => "MyString",
      :version => "MyString",
      :parent_id => 1
    ))
  end

  it "renders the edit model form" do
    render

    assert_select "form[action=?][method=?]", model_path(@model), "post" do

      assert_select "input[name=?]", "model[name]"

      assert_select "input[name=?]", "model[version]"

      assert_select "input[name=?]", "model[parent_id]"
    end
  end
end
