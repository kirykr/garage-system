require 'rails_helper'

RSpec.describe "models/index", type: :view do
  before(:each) do
    assign(:models, [
      Model.create!(
        :name => "Name",
        :version => "Version",
        :parent_id => 2
      ),
      Model.create!(
        :name => "Name",
        :version => "Version",
        :parent_id => 2
      )
    ])
  end

  it "renders a list of models" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Version".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
