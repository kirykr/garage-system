class User < ApplicationRecord
  attr_accessor :skip_password_validation, :skip_en_name_validation

  devise :invitable, :database_authenticatable, :recoverable, :rememberable, :validatable, :confirmable, :lockable, :trackable

  ROLES = %w(representer sport_representer)

  validates :given_name, :family_name, :given_name_kh, :family_name_kh, :email, presence: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }
  validate  :validate_role

  def full_name
    if family_name_kh.present? && given_name_kh.present?
      "#{family_name_kh} #{given_name_kh}"
    else
      "#{family_name} #{given_name}"
    end
  end

  protected
    def skip_en_name_validation?
      return false if skip_en_name_validation
      true
    end

    def password_required?
      return false if skip_password_validation
      super
    end

  private

    def validate_role
      if !persisted? && !role.present?
        errors.add(:role, 'must selected')
      end
    end
end
