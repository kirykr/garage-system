# Overriding the i18n_options method in the Devise FailureApp so that we can have control
# over how the `authentication_keys` parameter to the messages under devise.failure is translated.
# By default the `human_attribute_name` of the attribute is used, but those are typically capitalized,
# while the messages call for a lowercase value.
# This custom failure app must be activated in the config/initializers/devise.rb file.
class CustomDeviseFailureApp < Devise::FailureApp
  def route(scope)
    scope.to_sym == :user ? :new_user_session_url : :new_admin_session_url
  end

  def respond
    I18n.locale = params[:locale]
    super
  end

  # def i18n_options(options)
  #   if scope.to_sym == :user
  #     options.merge(authentication_keys: I18n.t("devise.failure.invalid"))
  #   else
  #     options.merge(authentication_keys: I18n.t("devise.failure.invalid"))
  #   end
  # end
end
