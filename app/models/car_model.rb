class CarModel < ApplicationRecord
  belongs_to :parent,           class_name: 'CarModel', optional: true
  has_many :children,           class_name: 'CarModel', foreign_key: 'parent_id', dependent: :destroy
  has_many :vehicles

  validates :name, presence: true

  scope :only_parents,  -> { where(parent_id: nil) }
  scope :only_children, -> { where.not(parent_id: nil) }
  scope :names,         -> { only_children.pluck(:name) }
end
