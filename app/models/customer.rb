class Customer < ApplicationRecord
  has_many :vehicles

  validates :name, :phone1, presence: true
end
