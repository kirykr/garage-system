class Vehicle < ApplicationRecord
  TYPES = %W(Sedan Hybrid 4WD SUV Coupe Convertible Limousine Pickup Luxury Microcar Truck)
  belongs_to :customer
  belongs_to :car_model

  validates :registration_number, presence: true
end
