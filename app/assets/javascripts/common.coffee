GMS.Common =
  init: ->
    @initNotification()
    @initSelect2()
    @checkFromValidation()

  initNotification: ->
    messageOption = {
      "closeButton": true,
      "debug": true,
      "progressBar": true,
      "positionClass": "toast-top-center",
      "showDuration": "400",
      "hideDuration": "1000",
      "timeOut": "7000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }

    messageInfo = $("#wrapper").data()

    if messageInfo and Object.keys(messageInfo).length > 0
      if messageInfo.messageType == 'notice'
        toastr.success(messageInfo.message, '', messageOption)
      else if messageInfo.messageType == 'alert'
        toastr.error(messageInfo.message, '', messageOption)

  initSelect2: ->
    $('.select2').select2 theme: 'bootstrap4'

  checkFromValidation: ->
    do ->
      'use strict'
      window.addEventListener 'load', (->
        # Fetch all the forms we want to apply custom Bootstrap validation styles to
        forms = document.getElementsByClassName('needs-validation')
        # Loop over them and prevent submission
        validation = Array::filter.call(forms, (form) ->
          form.addEventListener 'submit', ((event) ->
            if form.checkValidity() == false
              event.preventDefault()
              event.stopPropagation()
            form.classList.add 'was-validated'
            return
          ), false
          return
        )
        return
      ), false
      return


