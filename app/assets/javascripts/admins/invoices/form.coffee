#= require iCheck/icheck.min.js
#= require datapicker/bootstrap-datepicker.js

GMS.InvoicesNew = GMS.InvoicesCreate = GMS.InvoicesUpdate = GMS.InvoicesEdit = do ->
  _init = ->
    _initiCheck()
    _initDatePicker()
    _createCustomerAjax()
    _selectCustomerSelect2()

  _initiCheck = ->
    $('.i-checks').iCheck
      checkboxClass: 'icheckbox_square-green'
      radioClass: 'iradio_square-green'

  _initDatePicker = ->
    mem = $('.input-group.date').datepicker({
      todayBtn: "linked",
      keyboardNavigation: false,
      forceParse: false,
      calendarWeeks: true,
      autoclose: true
    });

  _createCustomerAjax = ->
    form = $('form#new_customer')

    $('#create-new-customer').on 'click', (e) ->
      e.preventDefault()
      $.ajax
        url: form.attr('action')
        type: 'post'
        dataType: 'json'
        data: form.serialize()
        processData: false
        success: (data, textStatus, jQxhr) ->
          form.removeClass('was-validated');
          $('#form-customer').modal('hide');
          form.trigger("reset");

          return
        error: (jqXhr, textStatus, errorThrown) ->
            console.log errorThrown
            form.addClass('was-validated');
            debugger;
            return
      return

  _selectCustomerSelect2 = ->
    $('#invoice_customer_id').select2
      theme: 'bootstrap4'
      ajax:
        url: '/admins/customers'
        dataType: 'json'
        type: 'GET'
        delay: 250
        data: (params) ->
          {
            q: params.term
            page: params.page
          }
        processResults: (data, params) ->
          params.page = params.page or 1
          {
            results: $.map(data.items, (item) ->
              {
                text: item.name
                id: item.id
              })
            pagination: more: params.page * 30 < data.total_count
          }
        cache: true

      placeholder: '-----Search for a customer---',
      minimumInputLength: 2

    return

  { init: _init }
