#= require iCheck/icheck.min.js
#= require datapicker/bootstrap-datepicker.js

GMS.VehiclesNew = GMS.VehiclesCreate = GMS.VehiclesUpdate = GMS.VehiclesEdit = do ->
  _init = ->
    _initiCheck()
    _initDatePicker()

  _initiCheck = ->
    $('.i-checks').iCheck
      checkboxClass: 'icheckbox_square-green'
      radioClass: 'iradio_square-green'

  _initDatePicker = ->
    mem = $('.input-group.date').datepicker({
      todayBtn: "linked",
      keyboardNavigation: false,
      forceParse: false,
      calendarWeeks: true,
      autoclose: true
    });


  { init: _init }
