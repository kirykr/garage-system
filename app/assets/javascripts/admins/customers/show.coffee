#= require slick/slick.min
GMS.CustomersShow = do ->
  _init = ->
    _initSlick()

  _initSlick = ->
    $('.product-images').slick dots: true

  { init: _init }
