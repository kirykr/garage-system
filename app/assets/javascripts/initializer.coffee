GMS.Initializer =
  exec: (pageName) ->
    if pageName && GMS[pageName]
      GMS[pageName]['init']()

  currentPage: ->
    return '' unless $('body').attr('id')

    bodyId      = $('body').attr('id').split('-')
    action      = GMS.Util.capitalize(bodyId[1])
    controller  = GMS.Util.capitalize(bodyId[0])
    controller + action

  init: ->
    GMS.Common.init()
    if @currentPage()
      GMS.Initializer.exec(@currentPage())

$(document).on 'ready turbolinks:load', ->
  GMS.Initializer.init()
