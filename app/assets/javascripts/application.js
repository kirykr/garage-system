// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
//
//= require jquery3
//= require jquery_ujs
//= require popper
//= require turbolinks
//= require bootstrap-sprockets
//= require metisMenu/jquery.metisMenu.js
//= require slimscroll/jquery.slimscroll.min.js
//= require inspinia.js
//= require toastr/toastr.min.js

//LOAD MODULE
//= require namespace
//= require util
//= require initializer
//= require common

//= require select2



