module ApplicationHelper
  def is_active_controller(controller_name, class_name = nil)
      if params[:controller] =~ /#{controller_name}/i
       class_name == nil ? "active" : class_name
      else
         nil
      end
  end

  def is_active_action(action_name)
      params[:action] == action_name ? "active" : nil
  end

  def stylesheet_exists?(controller_name)
    if ['index', 'show'].include?(action_name)
      ::Rails.application.assets.find_asset("#{controller_name}/#{action_name}.css")
    else
      ::Rails.application.assets.find_asset("#{controller_name}/form.css")
    end
  end

  def javascript_exists?(controller_name)
    if ['index', 'show'].include?(action_name)
      ::Rails.application.assets.find_asset("#{controller_name}/#{action_name}.js")
    else
      ::Rails.application.assets.find_asset("#{controller_name}/form.js")
    end
  end

  def flash_alert
    if notice
      { 'message-type': 'notice', 'message': notice }
    elsif alert
      { 'message-type': 'alert', 'message': alert }
    else
      {}
    end
  end

  def minit_side_bar?
    "#{controller_name}-#{action_name}" == 'invoices-new'
  end
end
