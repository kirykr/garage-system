class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_locale
  before_action :configure_permitted_parameters, if: :devise_controller?

  layout :layout

  def default_url_options
    { locale: I18n.locale }
  end

  protected

    def set_locale
      logger.debug "* Accept-Language: #{request.env['HTTP_ACCEPT_LANGUAGE']}"
      I18n.locale = extract_locale_from_accept_language_header
      logger.debug "* Locale set to '#{I18n.locale}'"
    end

    def authenticate_inviter!
      authenticate_admin!(force: true)
    end

  private

    def layout
      controller_names = %w(sessions passwords confirmations unlocks)
      controller_action_name = "#{controller_name}-#{action_name}"
      if controller_action_name == 'invitations-edit' || controller_action_name == 'invitations-update'
        'devise_confirm'
      elsif controller_names.include?(controller_name)
        'login_layout'
      else
        "application"
      end
    end

    def after_sign_in_path_for(resource_or_scope)
      stored_location_for(resource_or_scope) || super
    end

    def after_sign_out_path_for(resource_or_scope)
      request.referrer
    end

    def extract_locale_from_accept_language_header
      allowed_env = %w{km en} #english, km only
      env = (request.env['HTTP_ACCEPT_LANGUAGE'] || "").scan(/^[a-z]{2}/).first
      allowed_env.include?(env) ? env : env = "km" #default to english
    end

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(
        :invite, keys: [
          :skip_en_name_validation,
          :organization_id, :username,
          :given_name_kh, :given_name,
          :family_name_kh, :family_name,
          :role, :title
          ]
        )
      devise_parameter_sanitizer.permit(
        :accept_invitation, keys: [
          :skip_en_name_validation
        ]
      )
      devise_parameter_sanitizer.permit(:account_update, keys: [:family_name, :given_name, :family_name_kh, :given_name_kh, :phone_number, :role, :title, :organization_id])
    end
end
