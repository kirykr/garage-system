module Admins
  class UsersController < Admins::AdminsController
    before_action :find_user, only: [:show, :edit, :update, :destroy]

    def index
      respond_to do |f|
        f.html do
          @users = User.all
        end
        f.json { render json: User.all }
      end
    end

    def new
      @user = User.new
    end

    def create
      @user = User.new(user_params)
      if @user.save
        redirect_to admins_users_path, notice: t('.successfully_created')
      else
        render :new
      end
    end

    def show
    end

    def edit
    end

    def update
      if @user.update_attributes(user_params)
        redirect_to admins_user_path(@user), notice: t('.successfully_updated')
      else
        render :edit
      end
    end

    def destroy
      if @user.destroy
        redirect_to admins_users_url, notice: t('.successfully_deleted')
      else
        redirect_to admins_users_url, alert: t('.alert')
      end
    end

    private

      def user_params
        params.require(:user).permit(:given_name_kh, :family_name_kh, :given_name, :family_name, :email, :role, :title, :phone, :password, :password_confirmation)
      end

      def find_user
        @user = User.find(params[:id])
      end
  end
end
