class Admins::InvitationsController < Devise::InvitationsController
  def new
    @user = User.new(invite_params)
  end

  def create
    @user = User.invite!(invite_params, current_inviter)
    if @user.valid?
      redirect_to root_path, notice: I18n.t('invitations.send_instructions')
    else
      render :new, alert: I18n.t('invitations.send_instructions')
    end
  end

  private

    # this is called when creating invitation
    # should return an instance of resource class
    # def invite_resource
    #   # skip sending emails on invite
    #   # super { |user| user.skip_invitation = true }
    #   User.invite!(invite_params, current_inviter) # current_inviter will be set as invited_by
    # end

    # this is called when accepting invitation
    # should return an instance of resource class
    def accept_resource
      resource = resource_class.accept_invitation!(update_resource_params)
      # Report accepting invitation to analytics
      Analytics.report('invite.accept', resource.id)
      resource
    end
end
