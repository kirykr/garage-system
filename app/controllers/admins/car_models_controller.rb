module Admins
  class CarModelsController < Admins::AdminsController
    before_action :set_model, only: [:show, :edit, :update, :destroy]

    # GET /models
    # GET /models.json
    def index
      @car_models = CarModel.only_children
    end

    # GET /models/1
    # GET /models/1.json
    def show
    end

    # GET /models/new
    def new
      @car_model = CarModel.new
    end

    # GET /models/1/edit
    def edit
    end

    # POST /models
    # POST /models.json
    def create
      @car_model = CarModel.new(model_params)

      respond_to do |format|
        if @car_model.save
          format.html { redirect_to admins_car_model_path(@car_model), notice: 'CarModel was successfully created.' }
          format.json { render :show, status: :created, location: admins_car_model_path(@car_model) }
        else
          format.html { render :new }
          format.json { render json: @car_model.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /models/1
    # PATCH/PUT /models/1.json
    def update
      respond_to do |format|
        if @car_model.update(model_params)
          format.html { redirect_to admins_car_model_path(@car_model), notice: 'Car model was successfully updated.' }
          format.json { render :show, status: :ok, location: admins_car_model_path(@car_model) }
        else
          format.html { render :edit }
          format.json { render json: @car_model.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /models/1
    # DELETE /models/1.json
    def destroy
      @car_model.destroy
      respond_to do |format|
        format.html { redirect_to admins_car_models_url, notice: 'Car model was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_model
        @car_model = CarModel.find(params[:id])
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def model_params
        params.require(:car_model).permit(:name, :version, :parent_id)
      end
  end
end
