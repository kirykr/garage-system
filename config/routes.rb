Rails.application.routes.draw do
  # devise_for :admin, controllers: { sessions: 'admin/sessions' }
  devise_for :admins, path: 'admins', controllers: { sessions: 'admins/sessions', invitations: 'admins/invitations' }
  devise_for :users, skip: :registrations, controllers: { sessions: 'users/sessions' }

  # You can have the root of your site routed with "root"
  root to: 'dashboards#index'

  namespace 'admins' do
    get 'dashboard', to: 'dashboards#index'
    resources :users
    resources :customers
    resources :car_models
    resources :vehicles
    resources :invoices
  end

  get 'dashboard', to: 'dashboards#index'

  get "layoutsoptions/index"
  get "layoutsoptions/off_canvas"
end
