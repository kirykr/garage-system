require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

Bundler.require(*Rails.groups)

module SchoolSportAttendance
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    # config.autoload_paths += %W(#{config.root}/lib)
    config.i18n.available_locales = [:km, :en]
    config.time_zone = "Asia/Bangkok"
    # config.exceptions_app = self.routes

    # Don't generate system test files.
    config.generators do |g|
      g.template_engine :haml
      g.test_framework  :rspec, fixture: false
      g.stylesheets     false
      g.javascripts     false
      g.helper          false
      g.channel         assets: false
      g.jbuilder        false
    end
    config.generators.system_tests = nil
  end
end
