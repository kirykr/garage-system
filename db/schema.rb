# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_02_173820) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.integer "sign_in_count", default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "failed_attempts", default: 0
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.index ["email"], name: "index_admins_on_email", unique: true
    t.index ["invitation_token"], name: "index_admins_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_admins_on_invitations_count"
    t.index ["invited_by_id"], name: "index_admins_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_admins_on_invited_by_type_and_invited_by_id"
    t.index ["unlock_token"], name: "index_admins_on_unlock_token", unique: true
  end

  create_table "car_models", force: :cascade do |t|
    t.string "name"
    t.string "version"
    t.integer "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_car_models_on_name"
    t.index ["parent_id"], name: "index_car_models_on_parent_id"
  end

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.string "compay"
    t.string "address"
    t.string "email"
    t.string "phone1"
    t.string "phone2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_customers_on_name"
    t.index ["phone1"], name: "index_customers_on_phone1"
  end

  create_table "invoices", force: :cascade do |t|
    t.integer "reference_number"
    t.float "original_price"
    t.float "earning_value"
    t.float "total_price"
    t.float "discount_price"
    t.float "deposit_price"
    t.float "balance"
    t.bigint "user_id"
    t.bigint "customer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_invoices_on_customer_id"
    t.index ["reference_number"], name: "index_invoices_on_reference_number"
    t.index ["user_id"], name: "index_invoices_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.string "given_name_kh", default: ""
    t.string "family_name_kh", default: ""
    t.string "given_name", default: ""
    t.string "family_name", default: ""
    t.string "role", default: ""
    t.integer "organization_id"
    t.string "phone_number", default: ""
    t.string "title", default: ""
    t.string "profile_picture"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by_type_and_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  create_table "vehicles", force: :cascade do |t|
    t.string "model_year", limit: 5
    t.string "model_type", limit: 100
    t.string "registration_number", limit: 160
    t.string "nogears", limit: 32
    t.string "vin", limit: 32
    t.string "fuel_level", limit: 32
    t.string "color", limit: 64
    t.string "technician", limit: 120
    t.string "key_number", limit: 64
    t.string "enginesize", limit: 64
    t.string "engine_number", limit: 128
    t.string "gearbox_number", limit: 64
    t.string "odometerreading", limit: 64
    t.date "registration_date"
    t.date "alert_date"
    t.float "price", default: 0.0
    t.bigint "customer_id"
    t.bigint "car_model_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["car_model_id"], name: "index_vehicles_on_car_model_id"
    t.index ["customer_id"], name: "index_vehicles_on_customer_id"
    t.index ["registration_number"], name: "index_vehicles_on_registration_number"
  end

  add_foreign_key "invoices", "customers"
  add_foreign_key "invoices", "users"
  add_foreign_key "vehicles", "car_models"
  add_foreign_key "vehicles", "customers"
end
