class AddFieldsToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :given_name_kh, :string, default: ''
    add_column :users, :family_name_kh, :string, default: ''
    add_column :users, :given_name, :string, default: ''
    add_column :users, :family_name, :string, default: ''
    add_column :users, :role, :string, default: ''
    add_column :users, :organization_id, :integer
    add_column :users, :phone_number, :string, default: ''
    add_column :users, :title, :string, default: ''
    add_column :users, :profile_picture, :string
  end
end
