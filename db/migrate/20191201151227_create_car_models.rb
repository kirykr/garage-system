class CreateCarModels < ActiveRecord::Migration[5.2]
  def change
    create_table :car_models do |t|
      t.string :name
      t.string :version
      t.integer :parent_id

      t.timestamps
    end
    add_index :car_models, :name
    add_index :car_models, :parent_id
  end
end
