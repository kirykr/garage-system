class CreateVehicles < ActiveRecord::Migration[5.2]
  def change
    create_table :vehicles do |t|
      t.string :model_year, limit: 5
      t.string :model_type, limit: 100
      t.string :registration_number, limit: 160
      t.string :nogears, limit: 32
      t.string :vin, limit: 32
      t.string :fuel_level, limit: 32
      t.string :color, limit: 64
      t.string :technician, limit: 120
      t.string :key_number, limit: 64
      t.string :enginesize, limit: 64
      t.string :engine_number, limit: 128
      t.string :gearbox_number, limit: 64
      t.string :odometerreading, limit: 64
      t.date :registration_date
      t.date :alert_date
      t.float :price, default: 0.0
      t.references :customer, foreign_key: true
      t.references :car_model, foreign_key: true

      t.timestamps
    end
    add_index :vehicles, :registration_number
  end
end
