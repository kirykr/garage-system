class CreateCustomers < ActiveRecord::Migration[5.2]
  def change
    create_table :customers do |t|
      t.string :name
      t.string :compay
      t.string :address
      t.string :email
      t.string :phone1
      t.string :phone2

      t.timestamps
    end
    add_index :customers, :name
    add_index :customers, :phone1
  end
end
