class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.integer :reference_number
      t.float :original_price
      t.float :earning_value
      t.float :total_price
      t.float :discount_price
      t.float :deposit_price
      t.float :balance
      t.references :user, foreign_key: true
      t.references :customer, foreign_key: true

      t.timestamps
    end
    add_index :invoices, :reference_number
  end
end
